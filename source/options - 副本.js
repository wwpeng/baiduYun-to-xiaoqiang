var db = openDatabase("d2r", "0.1", "小米路由下载插件脚本存储", 200000);
function tab(menuname,tabboxname){
    var mname = 'active';
    var bname = 'show';
    var $li = $(menuname);
    var $listul = $(tabboxname);
    $li.eq(0).addClass(mname);
    $listul.eq(0).addClass(bname);
    $(menuname).click(function(){
        var index = $li.index(this);
        $li.removeClass(mname);
        $(this).addClass(mname);
        $listul.removeClass(bname);
        $listul.eq(index).addClass(bname);
        return false;
    })
}

$(function(){
    tab('.tabNav li','.tabBox .tabBoxLi');
    if(window.localStorage){
        var d2r = localStorage.getItem('d2r');
        d2r=d2r>0?true:false;
        $("[name=d2r]").attr('checked',d2r);
    }
    $("[name=d2r]").change(function(){
        localStorage.setItem('d2r',$(this).is(':checked')?1:-1);
    })

db.transaction( function(tx) {
        tx.executeSql(
            "SELECT * FROM script",
            [],
            function(tx, result){
                var rows = result.rows, length = rows.length, i=0;
                $('.scriptList tbody').empty();
                for(i; i < length; i++) {
                    var r = rows.item(i);
                    var status = rows.item(i)['status'];

                    var tr = [
                        $('<td>').html([$('<i class="state">').addClass(r.status=1?'on':'off').attr('title',r.status=1?'启用':'已禁用'),r.id]),
                        $('<td>').html(r.name),
                        $('<td>').html(r.version),
                        $('<td>').html($('<a>').attr('href',r.mail).html(r.author)),
                        $('<td>').html([
                            $('<a href="#" class="sta_but"></a>').html(r.status=1?'禁用':'启动'),
                            '<a href="#" class="upd_but">升级</a>',
                            '<a href="#" class="del_but">删除</a>'
                            ])
                    ];
                    $('.scriptList tbody').after($("<tr>").attr('id','scriptid'+r.id).html(tr));

                    


                }
            },
            function(tx, error){
                console.log('获取脚本列表时发生错误',error);
            }
        )
    });
    //scriptList
})